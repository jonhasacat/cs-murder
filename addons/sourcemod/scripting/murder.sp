#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <morecolors>
#include <smlib>
#include <entity>
#include <entity_prop_stocks>
#include <emitsoundany>

#include <murder>
#include murder/globals.sp
#include murder/integration.sp
#include murder/roles.sp
#include murder/weapons.sp
#include murder/victory.sp

public Plugin:myinfo =
{
	name = "CSGO Murder",
	author = "Jon",
	description = "CSGO Murder - Based on GM Murder",
	version = "1.0.3",
	url = "http://prestige-gaming.org"
}

public OnPluginStart()
{
	c_mu_debug = CreateConVar ("mu_debug", "0", "Enable Murder mu_debug Messages");
	AutoExecConfig(true);
	HookEvent("round_start", Murder_RoundStart, EventHookMode_Post);
	HookEvent("player_spawn", Murder_PlayerSpawn, EventHookMode_Post);
	HookEvent("round_end", Murder_RoundEndPre, EventHookMode_Pre);
	HookEvent("player_death", Murder_PlayerDeathPre, EventHookMode_Pre);

	g_FadeUserMsgId = GetUserMessageId("Fade");

	StartIntegration();
}

public OnMapStart() {
	AddFileToDownloadsTable("sound/custom/murder/bell.mp3");
	AddFileToDownloadsTable("sound/custom/murder/siren.mp3");
	AddFileToDownloadsTable("sound/custom/murder/scream.mp3");
	PrecacheSoundAny("sound/custom/murder/bell.mp3");
	PrecacheSoundAny("sound/custom/murder/siren.mp3");
	PrecacheSoundAny("sound/custom/murder/scream.mp3");

	g_iAlive = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	if (g_iAlive == -1)
		SetFailState("CCSPlayerResource.m_bAlive offset is invalid");

	g_iKills = FindSendPropInfo("CCSPlayerResource", "m_iKills");
	if (g_iKills == -1)
		SetFailState("CCSPlayerResource \"m_iKills\" offset is invalid");

	g_iDeaths = FindSendPropInfo("CCSPlayerResource", "m_iDeaths");
	if (g_iDeaths == -1)
		SetFailState("CCSPlayerResource \"m_iDeaths\"  offset is invalid");

	g_iAssists = FindSendPropInfo("CCSPlayerResource", "m_iAssists");
	if (g_iAssists == -1)
		SetFailState("CCSPlayerResource \"m_iAssists\"  offset is invalid");

	g_iMVPs = FindSendPropInfo("CCSPlayerResource", "m_iMVPs");
	if (g_iMVPs == -1)
		SetFailState("CCSPlayerResource \"m_iMVPs\"  offset is invalid");
	SDKHook(GetPlayerResourceEntity(), SDKHook_ThinkPost, ThinkPost);
	g_gun_timer = INVALID_HANDLE;
}

public void OnClientPutInServer(int client) {
	SDKHook(client, SDKHook_OnTakeDamageAlive, OnTakeDamageAlive);
	SDKHook(client, SDKHook_TraceAttack, OnTraceAttack);
	SDKHook(client, SDKHook_WeaponDrop, OnWeaponDrop);
	SDKHook(client, SDKHook_WeaponSwitch, OnWeaponSwitch);
	SDKHook(client, SDKHook_WeaponSwitchPost, OnWeaponSwitchPost);
	SDKHook(client, SDKHook_PostThinkPost, OnPostThinkPost);
	g_roles[client] = MU_ROLE_NONE;
}

public OnClientDisconnect_Post (int client) {
	if (IsPlayerGun(client)) {
		mu_debug("Gun Disconnect");
		gunTimer();
	}
	checkVictory();
}

public mu_debug (const String:msg[]) {
	if (GetConVarInt(c_mu_debug) != 1) return;
	PrintToChatAll("[mu_debug] %s", msg);
	PrintToServer("[mu_debug] %s", msg);
}

public Action:Murder_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	mu_debug("Murder_RoundStart - Start");
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i)) {
			Client_RemoveAllWeapons(i, "", true);
		}
	}
	g_role_timer = CreateTimer(10.0, TIMER_Roles);
	mu_debug("Murder_RoundStart - End");
}

public Action Murder_RoundEndPre(Event event, const char[] name, bool dontBroadcast)
{
	mu_debug("Murder_RoundEnd - Start");
	round_end = true;
	mu_debug ("reset");
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i)) {
			//Client_RemoveAllWeapons(i, "", true);
			g_armor[i] = false;
		}
	}
	roles_assigned = false;
	if (g_role_timer != INVALID_HANDLE) {
		KillTimer(g_role_timer);
		g_role_timer = INVALID_HANDLE;
	}
	mu_debug("Murder_RoundEnd - End");
}

public Action:Murder_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client != 0 && IsClientInGame(client) && IsPlayerAlive(client))
	{
		/*
		SetEntityRenderMode(client, RENDER_TRANSCOLOR);
		SetEntityRenderColor(client, 255, 255, 255, 255);
		CS_SetClientClanTag(client, "");
		*/
		if (roles_assigned && g_roles[client] == MU_ROLE_NONE) {
			CPrintToChat(client, "{green}[MURDER] {default}Round is currently ongoing. You will spawn next round.");
			ForcePlayerSuicide(client);
		}
		else {
			CPrintToChat(client, "{green}[MURDER] {default}Currently in Warmup. The round will begin in a few seconds.");
		}
	}
}

public Action OnTraceAttack(int iVictim, int &iAttacker, int &inflictor, float &damage, int &damagetype, int &ammotype, int hitbox, int hitgroup)
{
	if (iAttacker > MAXPLAYERS) {
		PrintToChatAll("U WOT M9 - %d", iAttacker);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action OnTakeDamageAlive(int iVictim, int &iAttacker, int &inflictor, float &damage, int &damagetype)
{
	if(!roles_assigned)
		return Plugin_Handled;
	if (damagetype == 128 || damagetype == 0 || damage == 0.0) {
		damage = 0.0;
		return Plugin_Changed;
	}
	else if (damagetype != 32 && iAttacker != 0) {
		damage = 100.0;
		if (IsPlayerMurderer(iVictim) && IsPlayerMurderer(iAttacker)) {
			CPrintToChat(iAttacker, "{green}[MURDER] {default}%N is your teammate", iVictim);
			damage = 0.0;
			return Plugin_Changed;
		} else if (g_armor[iVictim]) {
			g_armor[iVictim] = false;
			SetEntProp(iVictim, Prop_Data, "m_ArmorValue", 0, 1 );
			damage = 0.0;
			return Plugin_Changed;
		} else if (IsPlayerMurderer(iVictim)) {
			CPrintToChatAll("{green}[MURDER] {default}{red}%N{default} ({red}MURDERER{default}) was killed by {green}%N{default}!", iVictim, iAttacker);
			bystander_kill_FW(iAttacker);
		} else if (IsPlayerMurderer(iAttacker)) {
			murderer_kill_FW(iAttacker);
			PlayScream();
		} else if (IsPlayerBystander(iVictim) || IsPlayerGun(iVictim)) {
			Client_RemoveWeapon(iAttacker, "weapon_deagle", false);
			g_roles[iAttacker] = MU_ROLE_BYSTANDER;
			CPrintToChat(iAttacker, "{green}[MURDER] {default}You killed an innocent {blue}%N{default}!", iVictim);
			Blind(iAttacker);
			gunTimer();
			teammate_kill_FW(iAttacker);
		}
	}
	return Plugin_Changed;
}

public Action Murder_PlayerDeathPre(Event event, const char[] menu, bool dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (IsPlayerGun(client)) {
		mu_debug("Gun Death");
		g_roles[client] = MU_ROLE_BYSTANDER;
		gunTimer();
	}

	checkVictory();

	SetEventBroadcast(event, true);
	return Plugin_Changed;
}

public SetClientSpeed(client, Float:speed)
{
      SetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue", speed);
}

public void ThinkPost(int entity)
{
	int isAlive[65];

	GetEntDataArray(entity, g_iAlive, isAlive, 65);
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i) && PlayerHasRole(i) && !round_end) {
			isAlive[i] = true;
			CS_SetClientContributionScore(i, 0);
		}
	}

	SetEntDataArray(entity, g_iAlive, isAlive, 65);

	SetEntDataArray(entity, g_iKills, iZero, MaxClients + 1);
	SetEntDataArray(entity, g_iDeaths, iZero, MaxClients + 1);
	SetEntDataArray(entity, g_iAssists, iZero, MaxClients + 1);
	SetEntDataArray(entity, g_iMVPs, iZero, MaxClients + 1);
}

public OnPostThinkPost (client) {
	SetEntProp(client, Prop_Send, "m_iAddonBits", 0);
}

public void Blind (client) {
	new targets[2];
	targets[0] = client;

	new duration = 1536;
	new holdtime = 1536;
	new flags;
	flags = (0x0002 | 0x0008);

	new color[4] = { 0, 0, 0, 0 };
	color[3] = 255;

	Handle message = StartMessageEx(g_FadeUserMsgId, targets, 1);
	if (GetUserMessageType() == UM_Protobuf)
	{
		Protobuf pb = UserMessageToProtobuf(message);
		pb.SetInt("duration", duration);
		pb.SetInt("hold_time", holdtime);
		pb.SetInt("flags", flags);
		pb.SetColor("clr", color);
	}
	else
	{
		BfWrite bf = UserMessageToBfWrite(message);
		bf.WriteShort(duration);
		bf.WriteShort(holdtime);
		bf.WriteShort(flags);
		bf.WriteByte(color[0]);
		bf.WriteByte(color[1]);
		bf.WriteByte(color[2]);
		bf.WriteByte(color[3]);
	}

	EndMessage();
	CreateTimer(10.0, UnBlind, client);
}

public Action:UnBlind (Handle:timer, any:client) {
	new targets[2];
	targets[0] = client;

	new duration = 1536;
	new holdtime = 1536;
	new flags;
	flags = (0x0001 | 0x0010);

	new color[4] = { 0, 0, 0, 0 };
	color[3] = 0;

	Handle message = StartMessageEx(g_FadeUserMsgId, targets, 1);
	if (GetUserMessageType() == UM_Protobuf)
	{
		Protobuf pb = UserMessageToProtobuf(message);
		pb.SetInt("duration", duration);
		pb.SetInt("hold_time", holdtime);
		pb.SetInt("flags", flags);
		pb.SetColor("clr", color);
	}
	else
	{
		BfWrite bf = UserMessageToBfWrite(message);
		bf.WriteShort(duration);
		bf.WriteShort(holdtime);
		bf.WriteShort(flags);
		bf.WriteByte(color[0]);
		bf.WriteByte(color[1]);
		bf.WriteByte(color[2]);
		bf.WriteByte(color[3]);
	}

	EndMessage();
}
public PlayScream () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i))
			ClientCommand(i, "play */custom/murder/scream.mp3");
	}
}
public PlayBell () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i))
			ClientCommand(i, "play */custom/murder/bell.mp3");
	}
}
public PlaySiren () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i))
			ClientCommand(i, "play */custom/murder/siren.mp3");
	}
}
