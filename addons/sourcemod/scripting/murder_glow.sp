#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <morecolors>
#include <smlib>
#include <entity>
#include <murder>
#include <CustomPlayerSkins>

public Plugin:myinfo =
{
	name = "CSGO Murder - Glow",
	author = "Jon",
	description = "CSGO Murder Point Shop - For use with CSGO Murder",
	version = "1.0",
	url = "http://prestige-gaming.org"
}

public void OnPluginStart()
{
	CreateTimer(3.0, Timer_UpdateGlow, TIMER_REPEAT);

	HookEvent("player_spawn", Event_PlayerReset);
	HookEvent("player_death", Event_PlayerReset);
	//HookEvent("round_end", Event_RoundReset);
}

public Action Event_PlayerReset(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));

	if(IsClientConnected(client) && IsClientInGame(client))
		UnhookGlow(client);
}

public Action Event_RoundReset(Event event, const char[] name, bool dontBroadcast)
{
	for (new i = 1; i < MAXPLAYERS; i++)
	{
		if(IsClientConnected(i) && IsClientInGame(i))
			UnhookGlow(i);
	}
}

public Action Timer_UpdateGlow(Handle timer)
{
	for (new i = 1; i < MAXPLAYERS; i++)
	{
		if(IsClientConnected(i) && IsClientInGame(i)) {
			if (IsPlayerAlive(i))
				SetupGlowSkin(i);
			else {
				//UnhookGlow(i);
			}
		}
	}
}

public MU_OnRolesAssigned()
{
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i)) {
			SetupGlowSkin(i);
		}
	}
}

void SetupGlowSkin(int client)
{
	if(!MU_IsRoundActive())
		return;

	char sModel[PLATFORM_MAX_PATH];
	GetClientModel(client, sModel, sizeof(sModel));
	int iSkin = CPS_SetSkin(client, sModel, CPS_RENDER);

	if(iSkin == -1)
		return;

	if (SDKHookEx(iSkin, SDKHook_SetTransmit, OnSetTransmit_GlowSkin))
		SetupGlow(client, iSkin);
}

void SetupGlow(int client, int iSkin)
{
	int iOffset;

	if (!iOffset && (iOffset = GetEntSendPropOffs(iSkin, "m_clrGlow")) == -1)
		return;

	SetEntProp(iSkin, Prop_Send, "m_bShouldGlow", true, true);
	SetEntProp(iSkin, Prop_Send, "m_nGlowStyle", 0);
	SetEntPropFloat(iSkin, Prop_Send, "m_flGlowMaxDist", 10000000.0);

	int iRed = 0;
	int iGreen = 0;
	int iBlue = 0;

	if (MU_IsPlayerMurderer(client)) {
		iRed = 255;
	}

	SetEntData(iSkin, iOffset, iRed, _, true);
	SetEntData(iSkin, iOffset + 1, iGreen, _, true);
	SetEntData(iSkin, iOffset + 2, iBlue, _, true);
	SetEntData(iSkin, iOffset + 3, 255, _, true);
}

public Action OnSetTransmit_GlowSkin(int iSkin, int client)
{
	if(!MU_IsRoundActive())
		return Plugin_Handled;

	if(!IsPlayerAlive(client))
		return Plugin_Handled;
	if(!MU_IsPlayerMurderer(client))
		return Plugin_Handled;

	for (new i = 1; i < MAXPLAYERS; i++)
	{
		if(i < 1)
			continue;

		if (!IsClientConnected(i))
			continue;

		if (!IsClientInGame(i))
			continue;

		if(IsFakeClient(i))
			continue;

		if(!IsPlayerAlive(i))
			continue;

		if(!CPS_HasSkin(i))
			continue;

		if(EntRefToEntIndex(CPS_GetSkin(i)) != iSkin)
			continue;
		if(MU_IsPlayerMurderer(client))
			return Plugin_Continue;
	}

	return Plugin_Handled;
}

void UnhookGlow(int client)
{
	if(client < 1)
		return;
	if(!IsPlayerAlive(client))
		return;

	char sModel[PLATFORM_MAX_PATH];
	GetClientModel(client, sModel, sizeof(sModel));
	SetEntProp(CPS_SetSkin(client, sModel, CPS_RENDER), Prop_Send, "m_bShouldGlow", false, true);
	SDKUnhook(client, SDKHook_SetTransmit, OnSetTransmit_GlowSkin);
}
