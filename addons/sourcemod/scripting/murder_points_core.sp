#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <morecolors>
#include <smlib>
#include <entity>
#include <murder>
#include <murder_points>

new Handle:hDB;
new Handle:f_load_items;
new Handle:c_menu[MAXPLAYERS];
new g_points[MAXPLAYERS];
new g_points_delay[MAXPLAYERS];
new String:g_item_name[1024][128];
new Function:g_item_callback[1024];
new g_item_cost[1024];
new g_item_type[1024];
new Handle:g_item_plugin[1024];
new g_item_count = 0;

new String:log_path[256];

#define MAX_COMMUNITYID_LENGTH 18

public Plugin:myinfo =
{
	name = "CSGO Murder - Point Shop",
	author = "Jon",
	description = "CSGO Murder Point Shop - For use with CSGO Murder",
	version = "1.1-dev",
	url = "http://prestige-gaming.org"
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max) {
	CreateNative("MU_Points_RegisterItem", Native_MU_Points_RegisterItem);
}

// Register an item with the shop
// Returns g_item_count on success
// Returns -1 if too many items
// Returns 0 on Invalid Params
public int Native_MU_Points_RegisterItem (Handle plugin, int numParams) {
	if (g_item_count >= 1024)
		return -1;
	if (numParams != 4)
		return 0;
	new String:name[128];
	GetNativeString(1, name, 128);
	new points = GetNativeCell(2);
	if (points < 0)
		return 0;
	new type = GetNativeCell(3);
	new Function:callback = GetNativeFunction(4);
	strcopy(g_item_name[g_item_count], 128, name);
	g_item_cost[g_item_count] = points;
	g_item_type[g_item_count] = type;
	g_item_callback[g_item_count] = callback;
	g_item_plugin[g_item_count] = plugin;
	g_item_count++;
	return g_item_count;
}

public OnPluginStart () {

	RegConsoleCmd("sm_shop", Command_Shop, "Display the Murder Points shop.");
	RegConsoleCmd("sm_menu", Command_Shop, "Display the Murder Points shop.");
	RegAdminCmd("sm_givepoints", Command_GivePoints, ADMFLAG_ROOT);

	HookEvent("round_end", RoundEndPost, EventHookMode_Post);

	new String:error[512];
	hDB = SQLite_UseDatabase("murder", error, sizeof(error));
	new String:query[512];
	Format(query, sizeof(query), "CREATE TABLE IF NOT EXISTS `mu_points` (`ply_id` BIGINT(64) PRIMARY KEY, `points` INT(11))");
	if (!SQL_FastQuery(hDB, query)) {
		SQL_GetError(hDB, error, sizeof(error));
		CPrintToChatAll("db table create - %s", error);
	}
	BuildPath(Path_SM, log_path, sizeof(log_path), "logs/murder_points.log");
	f_load_items = CreateGlobalForward("MU_Points_OnLoadItems", ET_Ignore);
}

public OnAllPluginsLoaded() {
	Call_StartForward(f_load_items);
	Call_Finish();
}

forward MU_Points_OnLoadItems ();
public loadItems_FW() {
	Call_StartForward(f_load_items);
	Call_Finish();
}

public OnMapStart() {
}

public void OnClientPutInServer(int client) {
	if (!IsFakeClient(client)) {
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		new String:query[512];
		new String:error[512];
		Format(query, sizeof(query), "INSERT OR IGNORE INTO `mu_points` VALUES(%s, 0)", ply_id);
		if (!SQL_FastQuery(hDB, query)) {
			SQL_GetError(hDB, error, sizeof(error));
			LogToFile (log_path, "OCPIS ERROR 1 - %s - %s", ply_id, error);
		}
		Format(query, sizeof(query), "SELECT ply_id, points FROM `mu_points` WHERE ply_id = %s", ply_id);
		new Handle:result = SQL_Query(hDB, query);
		if (result == INVALID_HANDLE) {
			SQL_GetError(hDB, error, sizeof(error));
			LogToFile (log_path, "OCPIS ERROR 2 - %s - %s", ply_id, error);
		}
		SQL_FetchRow(result);
		g_points[client] = SQL_FetchInt(result, 1);
		g_points_delay[client] = 0;
		LogToFile(log_path, "OCPIS Load - %s - %d Points", ply_id, g_points[client]);
	} else {
		g_points[client] = 0;
		g_points_delay[client] = 0;
	}
}

public OnClientDisconnect (int client) {
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	new String:query[512];
	new String:error[512];
	Format(query, sizeof(query), "UPDATE `mu_points` SET points = %d WHERE ply_id = %s", g_points[client], ply_id);
	LogToFile(log_path, "OCD Save - %s - %d Points", ply_id, g_points[client]);
	if (!SQL_FastQuery(hDB, query)) {
		SQL_GetError(hDB, error, sizeof(error));
		LogToFile(log_path, "OCD Error - %s - %s", ply_id, error);
	}
}

public Action:Command_Shop (client, args)
{
	ShowClientShop(client);
	return Plugin_Handled;
}

public MU_OnRolesAssigned() {
	for (new i = 1; i < MAXPLAYERS; i++) {
		//ShowClientShop(i);
		if (MU_IsPlayerActive(i)) {
			CPrintToChat(i, "{green}[MURDER]{default} You have %d points. Type /menu to view the shop.", g_points[i]);
		}
	}
}

ClientCanBuyItem(client, item) {
	if (g_item_type[item] == TYPE_ALL)
		return true;
	if (g_item_type[item] == TYPE_MURDERER && MU_IsPlayerMurderer(client))
		return true;
	if (g_item_type[item] == TYPE_BYSTANDER && !MU_IsPlayerMurderer(client))
		return true;
	return false;
}

public ShowClientShop(client) {
	if (!MU_IsPlayerActive(client)) { return; }
	CPrintToChat(client, "{green}[MURDER]{default} You have %d points", g_points[client]);
	c_menu[client] = CreateMenu(PointMenuHandler);
	SetMenuTitle(c_menu[client], "Point Shop - %d Points", g_points[client]);
	for (new i = 0; i < g_item_count; i++) {
		if (ClientCanBuyItem(client, i)) {
			new String:msg[512];
			Format(msg, sizeof(msg), "%s - %d Points", g_item_name[i], g_item_cost[i]);
			new String:id[64];
			IntToString(i, id, sizeof(id));
			AddMenuItem(c_menu[client], id, msg);
		}
	}
	DisplayMenu(c_menu[client], client, 10);

}

public PointMenuHandler (Menu menu, MenuAction action, int param1, int param2) {
	if (action == MenuAction_End)
	{
		delete menu;
	}
	else if (action == MenuAction_Select)
	{
		if (!MU_IsRoundActive() || !MU_IsPlayerActive(param1)) {return;}
		char info[64];
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(param1, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

		menu.GetItem(param2, info, sizeof(info));
		new item = StringToInt(info);
		if (g_points[param1] >= g_item_cost[item] && ClientCanBuyItem(param1, item)) {
			g_points[param1] -= g_item_cost[item];
			LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, g_item_cost[item], g_points[param1]);
			CPrintToChat(param1, "{green}[MURDER]{default} Bought %s for %d points. %d Points Remaining", g_item_name[item], g_item_cost[item], g_points[param1]);
			Call_StartFunction(g_item_plugin[item], g_item_callback[item]);
			Call_PushCell(param1);
			Call_Finish();
		} else {
			CPrintToChat(param1, "{green}[MURDER]{default} You do not have enough points to buy %s. (%d/%d)", g_item_name[item], g_points[param1], g_item_cost[item]);
		}
	}
}

public Action RoundEndPost(Event event, const char[] name, bool dontBroadcast)
{
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i) && !IsFakeClient(i) && MU_GetPlayerRole(i) != 0) {
			if (IsPlayerAlive(i)) {
				delayPoints(i, 1);
			}
			updatePoints(i);
		}
	}
}

public Action:Command_GivePoints (client, args)
{
	new String:name[128];
	GetCmdArg(1, name, sizeof(name));
	new target = FindTarget(client, name);
	new String:s_points[128];
	GetCmdArg(2, s_points, sizeof(s_points));
	new points = StringToInt(s_points);
	g_points[target] += points;
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	LogToFile(log_path, "GP - %s - %d", ply_id, g_points[target]);
	ReplyToCommand(client, "Gave %N %d Points", target, points);
	return Plugin_Handled;
}

public delayPoints(client, points) {
	g_points_delay[client] += points;
}

public updatePoints(client) {
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	g_points[client] += g_points_delay[client];
	LogToFile(log_path, "uP - %s - %d", ply_id, g_points[client]);
	CPrintToChat(client, "{green}[MURDER] {default}You earned %d Points this round (Total: %d)", g_points_delay[client], g_points[client]);
	g_points_delay[client] = 0;
}

public MU_OnMurdererKill(client) {
	delayPoints(client, 1);
}

public MU_OnBystanderKill(client) {
	delayPoints(client, 3);
}

public MU_OnTeammateKill(client) {
	delayPoints(client, -1);
}
