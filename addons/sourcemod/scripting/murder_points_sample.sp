#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <smlib>
#include <entity>
#include <murder_points>

#define MAX_COMMUNITYID_LENGTH 18

public Plugin:myinfo =
{
	name = "CSGO Murder - Point Shop - Sample",
	author = "Jon",
	description = "CSGO Murder Point Shop Sample - For use with CSGO Murder Point Shop",
	version = "dev",
	url = "http://prestige-gaming.org"
}

public MU_Points_OnLoadItems() {
	MU_Points_RegisterItem("Smoke Grenade", 5, TYPE_ALL, BUY_Smoke);
}

public BUY_Smoke (int client) {
	GivePlayerItem(client, "weapon_smokegrenade");
}