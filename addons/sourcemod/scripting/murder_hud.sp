#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>

#include <murder>

char g_IconM[PLATFORM_MAX_PATH] = "murder/overlayMurderer";
char g_IconG[PLATFORM_MAX_PATH] = "murder/overlayGuardian";
char g_IconB[PLATFORM_MAX_PATH] = "murder/overlayBystander";

public Plugin:myinfo =
{
	name = "CSGO Murder HUD",
	author = "Jon",
	description = "CSGO Murder HUD",
	version = "dev",
	url = "http://prestige-gaming.org"
}

public void OnPluginStart()
{
  CreateTimer(0.3, Timer_UpdateText, _, TIMER_REPEAT);
  CreateTimer(5.0, Timer_UpdateOverlay, _, TIMER_REPEAT);

  HookEvent("round_prestart", Event_RoundStartPre, EventHookMode_Pre);
}

public void OnMapStart () {
  char sBuffer[PLATFORM_MAX_PATH];

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vmt", g_IconB);
  AddFileToDownloadsTable(sBuffer);

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vtf", g_IconB);
  AddFileToDownloadsTable(sBuffer);

  PrecacheDecal(g_IconB, true);

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vmt", g_IconG);
  AddFileToDownloadsTable(sBuffer);

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vtf", g_IconG);
  AddFileToDownloadsTable(sBuffer);

  PrecacheDecal(g_IconG, true);

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vmt", g_IconM);
  AddFileToDownloadsTable(sBuffer);

  Format(sBuffer, sizeof(sBuffer), "materials/%s.vtf", g_IconM);
  AddFileToDownloadsTable(sBuffer);

  PrecacheDecal(g_IconM, true);
}

public Action Event_RoundStartPre(Event event, const char[] name, bool dontBroadcast)
{
	ShowOverlayToAll(" ");
}

public int MU_OnRolesAssigned() {
  LoopValidClients(i)
    AssignOverlay(i);
}

public void AssignOverlay (int client) {
  int role = MU_GetPlayerRole(client);
  if (role == MU_ROLE_NONE) {
    ShowOverlayToClient(client, " ");
  } else if (!IsPlayerAlive(client)) {
    ShowOverlayToClient(client, " ");
  } else if (role == MU_ROLE_MURDERER) {
    ShowOverlayToClient(client, g_IconM);
  } else if (role == MU_ROLE_GUN) {
    ShowOverlayToClient(client, g_IconG);
  } else if (role == MU_ROLE_BYSTANDER) {
    ShowOverlayToClient(client, g_IconB);
  }
}
public Action Timer_UpdateOverlay(Handle timer) {
  LoopValidClients(i)
    AssignOverlay(i);
}

public Action Timer_UpdateText(Handle timer)
{
	LoopValidClients(client)
	{
		if (IsPlayerAlive(client))
		{
			int target = TraceClientViewEntity(client);

			if(!MU_IsClientValid(target))
				continue;

			if(!IsPlayerAlive(target))
				continue;

			if (MU_GetPlayerRole(client) == MU_ROLE_MURDERER)
			{
  			if (MU_GetPlayerRole(target) == MU_ROLE_MURDERER)
  				PrintHintText(client, "Player: <font color='#ff0000'>\"%N\"</font>", target); //red color
  			else
  				PrintHintText(client, "Player: <font color='#008000'>\"%N\"</font>", target); //green color
			}
			else
			{
				PrintHintText(client, "Player: \"%N\"", target); //default
			}
		}
		else
		{
			int iMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
			int iTarget = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");

			if (!MU_IsClientValid(iTarget))
				continue;

			if(!IsPlayerAlive(iTarget))
				continue;

			if(iMode == 4 || iMode == 5)
				PrintHintText(client, "Player: \"%N\"", iTarget);
		}
	}

	return Plugin_Continue;
}

stock int TraceClientViewEntity(int client)
{
	float m_vecOrigin[3];
	float m_angRotation[3];

	GetClientEyePosition(client, m_vecOrigin);
	GetClientEyeAngles(client, m_angRotation);

	Handle tr = TR_TraceRayFilterEx(m_vecOrigin, m_angRotation, MASK_SHOT, RayType_Infinite, TRDontHitSelf, client);
	int pEntity = -1;

	if (TR_DidHit(tr))
	{
		pEntity = TR_GetEntityIndex(tr);
		delete(tr);
		return pEntity;
	}

	if(tr != null)
		delete(tr);

	return -1;
}

public bool TRDontHitSelf(int entity, int mask, int data)
{
	return (1 <= entity <= MaxClients && entity != data);
}
