new Handle:f_OnResetRoles;
new Handle:f_roles_assigned;
new Handle:f_custom_roles;
new Handle:f_murderer_kill;
new Handle:f_bystander_kill;
new Handle:f_teammate_kill;
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max) {
	CreateNative("MU_IsPlayerActive", Native_MU_IsPlayerActive);
	CreateNative("MU_IsPlayerMurderer", Native_MU_IsPlayerMurderer);
	CreateNative("MU_IsRoundActive", Native_MU_IsRoundActive);
	CreateNative("MU_GetPlayerRole", Native_MU_GetPlayerRole);
	CreateNative("MU_GivePlayerArmor", Native_MU_GivePlayerArmor);
	CreateNative("MU_IsClientValid", Native_MU_IsClientValid);
	CreateNative("MU_SetPlayerRole", Native_MU_SetPlayerRole);
}
public StartIntegration() {
	f_OnResetRoles = CreateGlobalForward("MU_OnResetRoles", ET_Ignore);
	f_roles_assigned = CreateGlobalForward("MU_OnRolesAssigned", ET_Ignore);
	f_custom_roles = CreateGlobalForward("MU_AssignCustomRoles", ET_Ignore);
	f_murderer_kill = CreateGlobalForward("MU_OnMurdererKill", ET_Ignore, Param_Cell);
	f_bystander_kill = CreateGlobalForward("MU_OnBystanderKill", ET_Ignore, Param_Cell);
	f_teammate_kill = CreateGlobalForward("MU_OnTeammateKill", ET_Ignore, Param_Cell);
}
forward MU_OnResetRoles ();
public resetRoles_FW() {
	Call_StartForward(f_OnResetRoles);
	Call_Finish();
}
forward MU_OnRolesAssigned ();
public roles_assigned_FW() {
	Call_StartForward(f_roles_assigned);
	Call_Finish();
}
forward MU_AssignCustomRoles ();
public custom_roles_FW() {
	Call_StartForward(f_custom_roles);
	Call_Finish();
}
forward MU_OnMurdererKill(client);
public murderer_kill_FW (client) {
	Call_StartForward(f_murderer_kill);
	Call_PushCell(client);
	Call_Finish();
}
forward MU_OnBystanderKill(client);
public bystander_kill_FW(client) {
	Call_StartForward(f_bystander_kill);
	Call_PushCell(client);
	Call_Finish();
}
forward MU_OnTeammateKill(client);
public teammate_kill_FW(client) {
	Call_StartForward(f_teammate_kill);
	Call_PushCell(client);
	Call_Finish();
}

public int Native_MU_IsRoundActive (Handle plugin, int numParams) {
	return (roles_assigned && !round_end);
}

public int Native_MU_GetPlayerRole (Handle plugin, int NumParams) {
	int client = GetNativeCell(1);
	return g_roles[client];
}

public int Native_MU_IsPlayerActive (Handle plugin, int numParams) {
	int client = GetNativeCell(1);
	if (IsClientConnected(client) && IsClientInGame(client) && IsPlayerAlive(client))
		return true;
	return false;
}

public int Native_MU_IsClientValid (Handle plugin, int numParams) {
	int client = GetNativeCell(1);
	return (client > 0 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client));
}

public int Native_MU_IsPlayerMurderer (Handle plugin, int numParams) {
	int client = GetNativeCell(1);
	return (g_roles[client] == MU_ROLE_MURDERER);
}

public int Native_MU_GivePlayerArmor (Handle plugin, int numParams) {
	int client = GetNativeCell(1);
	g_armor[client] = true;
}

public int Native_MU_SetPlayerRole (Handle plugin, int numParams) {
	int client = GetNativeCell(1);
	int role = GetNativeCell(2);
	if (role == g_roles[client]){
		return;
	}
	if (role == MU_ROLE_MURDERER) {
		giveMurderer(client);
		notifyMurderer(client);
	} else if (role == MU_ROLE_GUN) {
		giveGun(client);
		notifyGun(client);
	} else if (role == MU_ROLE_BYSTANDER) {
		giveBystander(client);
		notifyBystander(client);
	} else {
		g_roles[client] = role;
	}
}
