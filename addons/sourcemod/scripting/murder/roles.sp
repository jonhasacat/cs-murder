public Action:TIMER_Roles (Handle timer, any data) {
	g_role_timer = INVALID_HANDLE;
	round_end = false;
	g_players = getActivePlayers();
	if (g_players < 2) {
		CPrintToChatAll ("{green}[MURDER] {default}Not enough players.");
		g_role_timer = CreateTimer(10.0, TIMER_Roles);
		return Plugin_Stop;
	}
	resetRoles();
	mu_debug("Roles are being assigned");
	new String:d[1024];
	Format(d, 1024, "g_players = %d", g_players);
	mu_debug(d);
	for (new i = 0; i < g_murderers; i++) {
		randomMurderer();
	}
	for (new i = 0; i < g_guns; i++) {
		randomGun();
	}
	custom_roles_FW();
	fillPlayers();
	notifyRoles();
	PlayScream();
	roles_assigned = true;
	roles_assigned_FW();
	return Plugin_Stop;
}

public resetRoles() {
	mu_debug("Role Reset Start");
	for (new i = 1; i < MAXPLAYERS; i++) {
		g_roles[i] = MU_ROLE_NONE;
		g_armor[i] = false;
		if (IsClientConnected(i) && IsClientInGame(i) && IsPlayerAlive(i))
			SetEntProp(i, Prop_Data, "m_ArmorValue", 0, 1 );
	}
	g_players_assigned = 0;
	if (getActivePlayers() > 20) {
		g_murderers = 3;
	} else if (getActivePlayers() > 12) {
		g_murderers = 2;
	} else {
		g_murderers = 1;
	}
	g_guns = 1;
	resetRoles_FW();
	mu_debug("Role Reset End");
}

public randomMurderer () {
	new random = GetRandomInt(1, g_players - g_players_assigned);
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i) && !PlayerHasRole(i)) {
			random--;
		}
		if (random == 0) {
			giveMurderer(i);
			g_players_assigned++;
			break;
		}
	}
}

public randomGun () {
	new random = GetRandomInt(1, g_players - g_players_assigned);
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i) && !PlayerHasRole(i)) {
			random--;
		}
		if (random == 0) {
			giveGun(i);
			g_players_assigned++;
			break;
		}
	}
}

public fillPlayers () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i) && !PlayerHasRole(i)) {
			giveBystander(i);
			g_players_assigned++;
		}
		if (MU_IsPlayerActive(i) && PlayerHasRole(i)) {
			Equip(i);
		}
	}
}

public notifyRoles() {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (!MU_IsPlayerActive(i)) {
			continue;
		}
		if (IsPlayerMurderer(i)) {
			notifyMurderer(i);
		} else if (IsPlayerGun(i)) {
			notifyGun(i);
		} else if (IsPlayerBystander(i)) {
			notifyBystander(i);
		}
	}
}

public notifyMurderer(client) {
	new String:buffer[1024];
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsPlayerMurderer(i)) {
			if (StrEqual(buffer, "")) {
				Format(buffer, 1024, "%N", i);
			} else {
				Format(buffer, 1024, "%s, %N", buffer, i);
			}
		}
	}
	CPrintToChat(client, "{green}[MURDER] {default}You are the {red}MURDERER{default}!");
	CPrintToChat(client, "{green}[MURDER] {default}Use your weapon to kill everyone!");
	CPrintToChat(client, "{green}[MURDER] {default}One of the others has a {green}secret weapon{default}, be careful.");
	if (g_murderers > 1) {
		CPrintToChat(client, "{green}[MURDER] {default}The {red}MURDERERS {default}are: %s", buffer);
	}
}

public notifyGun(client) {
	CPrintToChat(client, "{green}[MURDER] {default}You are a {green}Bystander with a Secret Weapon{default}!");
	CPrintToChat(client, "{green}[MURDER] {default}Find the {red}MURDERER{default} and use your weapon to kill them.");
	CPrintToChat(client, "{green}[MURDER] {default}Be careful not to kill {blue}innocents{default}.");
}

public notifyBystander(client) {
	CPrintToChat(client, "{green}[MURDER] {default}You are an {blue}innocent bystander{default}! Survive until the end of the round.");
}

public IsPlayerMurderer (client) {
	return (g_roles[client] == MU_ROLE_MURDERER);
}
public IsPlayerGun (client) {
	return (g_roles[client] == MU_ROLE_GUN);
}
public IsPlayerBystander (client) {
	return (g_roles[client] == MU_ROLE_BYSTANDER);
}

public PlayerHasRole(client) {
	return (g_roles[client] != MU_ROLE_NONE);
}

public getActivePlayers () {
	new players = 0;
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i)) {
			players++;
		}
	}
	return players;
}

public giveMurderer(client) {
	g_roles[client] = MU_ROLE_MURDERER;
	GivePlayerItem(client, "weapon_knife");
	mu_debug("Murderer Assigned");
}

public giveGun(client) {
	g_roles[client] = MU_ROLE_GUN;
	GivePlayerItem(client, "weapon_revolver");
	new w;
	w = GetPlayerWeaponSlot(client, 1);
	SetGunAmmo(client, w);
	SDKHook(w, SDKHook_Reload, OnGunReload);
	mu_debug("Gun Assigned");
}

public giveBystander(client) {
	g_roles[client] = MU_ROLE_BYSTANDER;
}

public getAliveMurderers() {
	new murderers = 0;
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i) && IsPlayerMurderer(i)) {
			murderers++;
		}
	}
	return murderers;
}
