public gunTimer() {
	if (g_gun_timer == INVALID_HANDLE) {
		g_gun_timer = CreateTimer(5.0, GunDelay, _, TIMER_FLAG_NO_MAPCHANGE);
	} else {
		mu_debug("gunTimer() Handle already exists");
	}
}

public Action:OnGunReload(weapon) {
	new m_iAmmo, m_iClip1, m_iPrimaryAmmoType;
	m_iAmmo = FindSendPropOffs("CBasePlayer", "m_iAmmo");
	m_iClip1 = FindSendPropOffs("CBaseCombatWeapon", "m_iClip1");
	m_iPrimaryAmmoType = FindSendPropOffs("CBaseCombatWeapon", "m_iPrimaryAmmoType");
	decl String:sWeapon[32];
	GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
	if (StrEqual(sWeapon, "weapon_deagle")) {
		new WeaponID = GetEntData(weapon, m_iPrimaryAmmoType) * 4;
		SetEntData(Weapon_GetOwner(weapon), m_iAmmo + WeaponID, 1);
		if (GetEntData(weapon, m_iClip1, 4) > 0) {
			//PrintToChatAll("Blocking Reload %N", g_gun);
			return Plugin_Handled;
		}
	}
    //PrintToChatAll("Allowing Reload %N", g_gun);
	CreateTimer(2.5, SetGunAmmo2, weapon);
	return Plugin_Continue;
}

public SetGunAmmo (client, weapon) {
	new m_iAmmo, m_iClip1, m_iClip2, m_iPrimaryAmmoType;
	m_iAmmo              = FindSendPropOffs("CBasePlayer",       "m_iAmmo");
	m_iClip1             = FindSendPropOffs("CBaseCombatWeapon", "m_iClip1");
	m_iClip2             = FindSendPropOffs("CBaseCombatWeapon", "m_iClip2");
	m_iPrimaryAmmoType   = FindSendPropOffs("CBaseCombatWeapon", "m_iPrimaryAmmoType");
	decl String:sWeapon[32];
	GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
	if (StrEqual(sWeapon, "weapon_deagle")) {
		//PrintToChatAll("SetGunAmmo %N weapon_deagle", client, weapon);
		new WeaponID = GetEntData(weapon, m_iPrimaryAmmoType) * 4;
		SetEntData(client, m_iAmmo + WeaponID, 0);
		SetEntData(weapon, m_iClip2, 0);
		SetEntData(weapon, m_iClip1, 1);
	}
}

public Action:SetGunAmmo2 (Handle: timer, any:weapon) {
	if (!IsValidEntity(weapon)) return Plugin_Stop;
	if (!MU_IsPlayerActive(Weapon_GetOwner(weapon))) return Plugin_Stop;
	new m_iAmmo, m_iPrimaryAmmoType;
	m_iAmmo              = FindSendPropOffs("CBasePlayer",       "m_iAmmo");
	m_iPrimaryAmmoType   = FindSendPropOffs("CBaseCombatWeapon", "m_iPrimaryAmmoType");
	decl String:sWeapon[32];
	GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
	//PrintToChatAll("SetGunAmmo2 %N", g_gun);
	if (StrEqual(sWeapon, "weapon_deagle")) {
		new WeaponID = GetEntData(weapon, m_iPrimaryAmmoType) * 4;
		SetEntData(Weapon_GetOwner(weapon), m_iAmmo + WeaponID, 1);
	}
	return Plugin_Continue;
}

public Equip (player) {
	GivePlayerItem(player, "weapon_decoy");
	//GivePlayerItem(player, "weapon_knife");
}

public Action:OnPlayerRunCmd(iClient, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if ( buttons & IN_ATTACK )
	{
		new String:buf[64];
		GetClientWeapon(iClient, buf, 64);
		if (StrEqual("weapon_decoy", buf))
			buttons &= ~IN_ATTACK;
	}
	if ( buttons & IN_ATTACK2 )
	{
		new String:buf[64];
		GetClientWeapon(iClient, buf, 64);
		if (StrEqual("weapon_decoy", buf))
			buttons &= ~IN_ATTACK2;
	}

	return Plugin_Continue;
}

public Action:OnWeaponDrop (client, weapon) {
	if (IsValidEdict(weapon) && IsValidEntity(weapon)) {
		decl String:sWeapon[32];
		GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
		if (IsPlayerGun(client) && StrEqual (sWeapon, "weapon_deagle")) {
			mu_debug("Gun Dropped");
			gunTimer();
			Entity_Kill(weapon);
		}
	}
}

public Action GunDelay (Handle timer, any client) {
	g_gun_timer = INVALID_HANDLE;
	if (!roles_assigned)
		return Plugin_Stop;
	new random = GetRandomInt(1, getActivePlayers() - getAliveMurderers());
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (MU_IsPlayerActive(i) && !IsPlayerMurderer(i)) {
			random--;
		}
		if (random == 0) {
			giveGun(i);
			break;
		}
	}
	return Plugin_Stop;
}

public Action:OnWeaponSwitch(client, weapon)
{
    decl String:sWeapon[32];
    GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));

    if(StrEqual(sWeapon, "weapon_knife") && g_roles[client] != MU_ROLE_MURDERER)
        return Plugin_Handled;

    return Plugin_Continue;
}

public OnWeaponSwitchPost(client, weapon) {
	new String:weapon2[24];
	GetClientWeapon(client,weapon2,24);//find out what weapon is in his hand
	if (StrEqual(weapon2,"weapon_knife")) {//if he is hoding a knife
        SetClientSpeed(client,1.1);//set the speed, "speed" is the value you need to set
    }
	else { //if he is hoding something other
		SetClientSpeed(client,1.0);//set to default
	}
}
