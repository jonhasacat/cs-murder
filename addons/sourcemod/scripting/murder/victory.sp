public checkVictory () {
	if (getAliveMurderers() < 1)  {
		bystanderVictory();
	}
	if (getActivePlayers() <= getAliveMurderers()) {
		mu_debug ("alive <= murderers");
		murdererVictory();
	}
}

public murdererVictory () {
	new String:buffer[1024];
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsPlayerMurderer(i) && IsClientConnected(i) && IsClientInGame(i)) {
			if (StrEqual(buffer, "")) {
				Format(buffer, 1024, "%N", i);
			} else  {
				Format(buffer, 1024, "%s, %N", buffer, i);
			}
		}
	}
	PlayBell();
	CPrintToChatAll("{green}[MURDER] {default}{red}MURDERERS{default} (%s) have won the round!", buffer);
	CS_TerminateRound(5.0, CSRoundEnd_TerroristWin, false);
}

public bystanderVictory() {
	new String:buffer[1024];
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i) && IsPlayerMurderer(i)) {
			if (StrEqual(buffer, "")) {
				Format(buffer, 1024, "%N", i);
			} else  {
				Format(buffer, 1024, "%s, %N", buffer, i);
			}
		}
	}
	PlayBell();
	CPrintToChatAll("{green}[MURDER] {default}Bystanders win! The murderers were %s", buffer);
	mu_debug("no murderers alive");
	CS_TerminateRound(5.0, CSRoundEnd_CTWin, false);
}