#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <smlib>
#include <entity>
#include <murder>
#include <murder_points>
#include <emitsoundany>

#define MAX_COMMUNITYID_LENGTH 18

public Plugin:myinfo =
{
	name = "CSGO Murder - Point Shop - Base Items",
	author = "Jon",
	description = "CSGO Murder Point Shop Base Items - For use with CSGO Murder Point Shop",
	version = "1.0-dev",
	url = "http://prestige-gaming.org"
}

public OnPluginStart () {

}

public OnMapStart() {
	AddFileToDownloadsTable("sound/custom/murder/siren.mp3");
	PrecacheSoundAny("sound/custom/murder/siren.mp3");
}

public MU_Points_OnLoadItems() {
	MU_Points_RegisterItem("Smoke Grenade", 5, TYPE_ALL, BUY_Smoke);
	MU_Points_RegisterItem("Flashbang Grenade", 5, TYPE_ALL, BUY_Flash);
	MU_Points_RegisterItem("Armor", 25, TYPE_MURDERER, BUY_Armor);
	MU_Points_RegisterItem("Grenade", 10, TYPE_MURDERER, BUY_Grenade);
	MU_Points_RegisterItem("Molotov", 20, TYPE_MURDERER, BUY_Molotov);
	MU_Points_RegisterItem("Negev", 5000, TYPE_ALL, BUY_Negev);
	MU_Points_RegisterItem("Tactical Awareness Grenade", 15, TYPE_MURDERER, BUY_TAGrenade);
}

public BUY_Smoke (int client) {
	GivePlayerItem(client, "weapon_smokegrenade");
}

public BUY_Flash (int client) {
	GivePlayerItem(client, "weapon_flashbang");
}

public BUY_Armor (int client) {
	SetEntProp(client, Prop_Data, "m_ArmorValue", 100, 1 );
	MU_GivePlayerArmor(client);
}

public BUY_Grenade (int client) {
	GivePlayerItem (client, "weapon_hegrenade");
}

public BUY_Molotov (int client) {
	GivePlayerItem (client, "weapon_molotov");
}

public BUY_Negev (int client) {
	GivePlayerItem (client, "weapon_negev");
	MU_SetPlayerRole(client, MU_ROLE_MURDERER);
	PlaySiren();
}

public PlaySiren () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i))
			ClientCommand(i, "play */custom/murder/siren.mp3");
	}
}

public BUY_TAGrenade (int client) {
	GivePlayerItem(client, "weapon_tagrenade");
}
