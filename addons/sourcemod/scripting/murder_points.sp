#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <morecolors>
#include <smlib>
#include <entity>
#include <murder>
#include <emitsoundany>

new Handle:hDB;
new Handle:c_mu_points_smoke;
new Handle:c_mu_points_flash;
new Handle:c_mu_points_armor;
new Handle:c_mu_points_grenade;
new Handle:c_mu_points_molotov;
new Handle:c_mu_points_negev;
new Handle:c_menu[MAXPLAYERS];
new g_points[MAXPLAYERS];
new g_points_delay[MAXPLAYERS];

new String:log_path[256];

#define MAX_COMMUNITYID_LENGTH 18

public Plugin:myinfo =
{
	name = "CSGO Murder - Point Shop",
	author = "Jon",
	description = "CSGO Murder Point Shop - For use with CSGO Murder",
	version = "1.0.1",
	url = "http://prestige-gaming.org"
}

public OnPluginStart () {
	c_mu_points_smoke = CreateConVar ("mu_points_smoke", "5", "Murder Points Cost of a Smoke Grenade");
	c_mu_points_flash = CreateConVar ("mu_points_flash", "5", "Murder Points Cost of a Flashbang Grenade");
	c_mu_points_armor = CreateConVar ("mu_points_armor", "25", "Murder Points Cost of Armor");
	c_mu_points_grenade = CreateConVar ("mu_points_grenade", "10", "Murder Points Cost of Grenade");
	c_mu_points_molotov = CreateConVar ("mu_points_molotov", "20", "Murder Points Cost of Molotov");
	c_mu_points_negev = CreateConVar ("mu_points_negev", "10000", "Murder Points Cost of Negev");

	RegConsoleCmd("sm_shop", Command_Shop, "Display the Murder Points shop.");
	RegConsoleCmd("sm_menu", Command_Shop, "Display the Murder Points shop.");

	HookEvent("round_end", RoundEndPost, EventHookMode_Post);

	new String:error[512];
	hDB = SQLite_UseDatabase("murder", error, sizeof(error));
	new String:query[512];
	Format(query, sizeof(query), "CREATE TABLE IF NOT EXISTS `mu_points` (`ply_id` BIGINT(64) PRIMARY KEY, `points` INT(11))");
	if (!SQL_FastQuery(hDB, query)) {
		SQL_GetError(hDB, error, sizeof(error));
		CPrintToChatAll("db table create - %s", error);
	}
	BuildPath(Path_SM, log_path, sizeof(log_path), "logs/murder_points.log");
}

public OnMapStart() {
	AddFileToDownloadsTable("sound/custom/murder/siren.mp3");
	PrecacheSoundAny("sound/custom/murder/siren.mp3");
}

public void OnClientPutInServer(int client) {
	if (!IsFakeClient(client)) {
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		new String:query[512];
		new String:error[512];
		Format(query, sizeof(query), "INSERT OR IGNORE INTO `mu_points` VALUES(%s, 0)", ply_id);
		if (!SQL_FastQuery(hDB, query)) {
			SQL_GetError(hDB, error, sizeof(error));
			LogToFile (log_path, "OCPIS ERROR 1 - %s - %s", ply_id, error);
		}
		Format(query, sizeof(query), "SELECT ply_id, points FROM `mu_points` WHERE ply_id = %s", ply_id);
		new Handle:result = SQL_Query(hDB, query);
		if (result == INVALID_HANDLE) {
			SQL_GetError(hDB, error, sizeof(error));
			LogToFile (log_path, "OCPIS ERROR 2 - %s - %s", ply_id, error);
		}
		SQL_FetchRow(result);
		g_points[client] = SQL_FetchInt(result, 1);
		g_points_delay[client] = 0;
		LogToFile(log_path, "OCPIS Load - %s - %d Points", ply_id, g_points[client]);
	} else {
		g_points[client] = 0;
		g_points_delay[client] = 0;
	}
}

public OnClientDisconnect (int client) {
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	new String:query[512];
	new String:error[512];
	Format(query, sizeof(query), "UPDATE `mu_points` SET points = %d WHERE ply_id = %s", g_points[client], ply_id);
	LogToFile(log_path, "OCD Save - %s - %d Points", ply_id, g_points[client]);
	if (!SQL_FastQuery(hDB, query)) {
		SQL_GetError(hDB, error, sizeof(error));
		LogToFile(log_path, "OCD Error - %s - %s", ply_id, error);
	}
}

public Action:Command_Shop (client, args)
{
	ShowClientShop(client);
	return Plugin_Handled;
}

public MU_OnRolesAssigned() {
	for (new i = 1; i < MAXPLAYERS; i++) {
		//ShowClientShop(i);
		if (MU_IsPlayerActive(i)) {
			CPrintToChat(i, "{green}[MURDER]{default} You have %d points. Type /menu to view the shop.", g_points[i]);
		}
	}
}

public ShowClientShop(client) {
	if (!MU_IsPlayerActive(client)) { return; }
	CPrintToChat(client, "{green}[MURDER]{default} You have %d points", g_points[client]);
	c_menu[client] = CreateMenu(PointMenuHandler);
	SetMenuTitle(c_menu[client], "Point Shop - %d Points", g_points[client]);
	new String:msg[512];
	Format(msg, sizeof(msg), "Smoke Grenade - %d Points", GetConVarInt(c_mu_points_smoke));
	AddMenuItem(c_menu[client], "weapon_smokegrenade", msg);
	Format(msg, sizeof(msg), "Flashbang - %d Points", GetConVarInt(c_mu_points_flash));
	AddMenuItem(c_menu[client], "weapon_flashbang", msg);
	if (MU_IsPlayerMurderer(client)) {
		Format(msg, sizeof(msg), "Armor - %d Points", GetConVarInt(c_mu_points_armor));
		AddMenuItem(c_menu[client], "armor", msg);
		Format(msg, sizeof(msg), "Grenade - %d Points", GetConVarInt(c_mu_points_grenade));
		AddMenuItem(c_menu[client], "weapon_hegrenade", msg);
		Format(msg, sizeof(msg), "Molotov - %d Points", GetConVarInt(c_mu_points_molotov));
		AddMenuItem(c_menu[client], "weapon_molotov", msg);
	}
	Format(msg, sizeof(msg), "Negev - %d Points", GetConVarInt(c_mu_points_negev));
	AddMenuItem(c_menu[client], "weapon_negev", msg);
	DisplayMenu(c_menu[client], client, 10);

}

public PointMenuHandler (Menu menu, MenuAction action, int param1, int param2) {
	if (action == MenuAction_End)
	{
		delete menu;
	}
	else if (action == MenuAction_Select)
	{
		if (!MU_IsRoundActive() || !MU_IsPlayerActive(param1)) {return;}
		char info[64];
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(param1, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

		menu.GetItem(param2, info, sizeof(info));
		new cost;
		if (StrEqual(info, "weapon_smokegrenade")) {
			cost = GetConVarInt(c_mu_points_smoke);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Smoke Grenade for %d points. %d Points Remaining", cost, g_points[param1]);
				GivePlayerItem(param1, info);
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for a Smoke Grenade. (%d/%d)", g_points[param1], cost);
			}
		} else if (StrEqual(info, "weapon_flashbang")) {
			cost = GetConVarInt(c_mu_points_flash);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Flashbang for %d points. %d Points Remaining", cost, g_points[param1]);
				GivePlayerItem(param1, info);
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for a Flashbang. (%d/%d)", g_points[param1], cost);
			}
		} else if (StrEqual(info, "armor") && MU_IsPlayerMurderer(param1)) {
			cost = GetConVarInt(c_mu_points_armor);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Armor for %d points. %d Points Remaining", cost, g_points[param1]);
				SetEntProp(param1, Prop_Data, "m_ArmorValue", 100, 1 );
				MU_GivePlayerArmor(param1);
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for Armor. (%d/%d)", g_points[param1], cost);
			}
		} else if (StrEqual(info, "weapon_hegrenade") && MU_IsPlayerMurderer(param1)) {
			cost = GetConVarInt(c_mu_points_grenade);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Grenade for %d points. %d Points Remaining", cost, g_points[param1]);
				GivePlayerItem(param1, info);
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for Grenade. (%d/%d)", g_points[param1], cost);
			}
		} else if (StrEqual(info, "weapon_molotov") && MU_IsPlayerMurderer(param1)) {
			cost = GetConVarInt(c_mu_points_molotov);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Molotov for %d points. %d Points Remaining", cost, g_points[param1]);
				GivePlayerItem(param1, info);
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for Molotov. (%d/%d)", g_points[param1], cost);
			}
		} else if (StrEqual(info, "weapon_negev")) {
			cost = GetConVarInt(c_mu_points_negev);
			if (g_points[param1] >= cost) {
				g_points[param1] -= cost;
				LogToFile(log_path, "Shop - %s - Spent %d - %d Points", ply_id, cost, g_points[param1]);
				CPrintToChat(param1, "{green}[MURDER] {default} Bought Negev for %d points. %d Points Remaining", cost, g_points[param1]);
				GivePlayerItem(param1, info);
				PlaySiren();
			} else {
				CPrintToChat(param1, "{green}[MURDER] {default} You do not have enough points for Molotov. (%d/%d)", g_points[param1], cost);
			}
		}
	}
}

public Action RoundEndPost(Event event, const char[] name, bool dontBroadcast)
{
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i) && !IsFakeClient(i) && MU_GetPlayerRole(i) != 0) {
			if (IsPlayerAlive(i)) {
				delayPoints(i, 1);
			}
			updatePoints(i);
		}
	}
}

public delayPoints(client, points) {
	g_points_delay[client] += points;
}

public updatePoints(client) {
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	g_points[client] += g_points_delay[client];
	LogToFile(log_path, "uP - %s - %d", ply_id, g_points[client]);
	CPrintToChat(client, "{green}[MURDER] {default}You earned %d Points this round (Total: %d)", g_points_delay[client], g_points[client]);
	g_points_delay[client] = 0;
}

public MU_OnMurdererKill(client) {
	delayPoints(client, 1);
}

public MU_OnBystanderKill(client) {
	delayPoints(client, 3);
}

public MU_OnTeammateKill(client) {
	delayPoints(client, -1);
}

public PlaySiren () {
	for (new i = 1; i < MAXPLAYERS; i++) {
		if (IsClientConnected(i) && IsClientInGame(i))
			ClientCommand(i, "play */custom/murder/siren.mp3");
	}
}
