#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <smlib>
#include <entity>
#include <murder_points>

public Plugin:myinfo =
{
	name = "CSGO Murder - Point Shop - Base Items",
	author = "Jon",
	description = "CSGO Murder Point Shop Base Items - For use with CSGO Murder Point Shop",
	version = "1.0-dev",
	url = "http://prestige-gaming.org"
}

public MU_Points_OnLoadItems() {
	MU_Points_RegisterItem("Freeze Scout", 20, TYPE_ALL, BUY_Scout);
}

public BUY_Scout (int client) {
	GivePlayerItem(client, "weapon_ssg08");
}